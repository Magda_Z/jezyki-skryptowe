from random import randint
import time

long_list = [randint(0, 3000) for element in range(1000000)]

start = time.time()
for i in range(len(long_list)):
    if long_list[i] == -1:
        break;
end = time.time()
print("1.Potrzebny czas: "+ str(end-start)+"s")

start = time.time()
try:
    long_list.index(-1)

except:
    pass    #pass jest pusta instrukcja, nic nie robi, ale nie powoduje problemu przy kompilacji w przeciwienstwie do nie wpisania zadnej instrukcji
end = time.time()
print("2.Potrzebny czas: "+ str(end-start)+"s")
