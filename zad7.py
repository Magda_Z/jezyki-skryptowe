def person_printERROR(name, last_name, *others, age):             #Taki zapis wywola blad poniewaz "*other" przysloni argument "age"
    formatted_data='Imie: {}, nazwisko: {}, wiek: {}'.format(name,last_name, age)
    others_str=''
    for arg in others:
        others_str+=arg+''
    print(formatted_data+others_str)

def person_printGOOD(name, last_name, age, *others):             #Taki zapis jest poprawny
    formatted_data='Imie: {}, nazwisko: {}, wiek: {} '.format(name,last_name, age)
    others_str=''
    for arg in others:
        others_str+=arg+' '
    print(formatted_data+others_str)


def addManyNumbers(*nums):      #przyklad *args
    sum = 0
    for num in nums:
        sum += num
    return sum

def createASentence(**words):  #przyklad **kwargs, przechodzenie po slowniku, jezeli chcemy uzyskac wartosci musimy uzyc values(), jezeli chcemy klucze to nie potrzebujemy values()
    res = ""
    for arg in words.values():
        res += arg
    return res


person_printGOOD("Jan", "Kowalski", "30", "Zainteresowania:", "Matematyka", "Informatyka")
print(addManyNumbers(5, 4, 2, 7, 1, 3))
print(addManyNumbers(20, 51, 53, 12, 7, 8, 4, 6, 3, 6,88))
print(createASentence(a="Zadania ", b="z ", c="Pythona"))