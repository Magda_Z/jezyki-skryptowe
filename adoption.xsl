<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">

    <html>
        <head>
            <title>Schronisko dla zwierz�t</title>
        </head>

        <body>
            <xsl:for-each select="/adoption/animal">
                <div>
                    <img>
                        <xsl:attribute name="src">
                            <xsl:value-of select="photo"/>
                        </xsl:attribute>
                    </img>
                <xsl:choose>
                    <xsl:when test="@category='puppies'">
                        <span style="color:blue">
                            <xsl:value-of select="name" /> ( <xsl:value-of select="age"/> )

                        </span>
                    </xsl:when>

                    <xsl:when test="@category='adult'">
                        <span style="color:green">
                            <xsl:value-of select="name" /> ( <xsl:value-of select="age"/> )

                        </span>
                    </xsl:when>

                    <xsl:when test="@category='senior'">
                        <span style="color:red">
                            <xsl:value-of select="name" /> ( <xsl:value-of select="age"/> )

                        </span>
                    </xsl:when>

                </xsl:choose>
                </div>
            </xsl:for-each>
        </body>
    </html>
    </xsl:template>

</xsl:stylesheet>